// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'settings.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ButterflySettingsTearOff {
  const _$ButterflySettingsTearOff();

  _ButterflySettings call(
      {ThemeMode theme = ThemeMode.system,
      String localeTag = '',
      String documentPath = '',
      String dateFormat = '',
      double touchSensitivity = 1,
      double mouseSensitivity = 1,
      double penSensitivity = 1,
      double selectSensitivity = 5,
      InputType inputType = InputType.multiDraw,
      String? lastVersion}) {
    return _ButterflySettings(
      theme: theme,
      localeTag: localeTag,
      documentPath: documentPath,
      dateFormat: dateFormat,
      touchSensitivity: touchSensitivity,
      mouseSensitivity: mouseSensitivity,
      penSensitivity: penSensitivity,
      selectSensitivity: selectSensitivity,
      inputType: inputType,
      lastVersion: lastVersion,
    );
  }
}

/// @nodoc
const $ButterflySettings = _$ButterflySettingsTearOff();

/// @nodoc
mixin _$ButterflySettings {
  ThemeMode get theme => throw _privateConstructorUsedError;
  String get localeTag => throw _privateConstructorUsedError;
  String get documentPath => throw _privateConstructorUsedError;
  String get dateFormat => throw _privateConstructorUsedError;
  double get touchSensitivity => throw _privateConstructorUsedError;
  double get mouseSensitivity => throw _privateConstructorUsedError;
  double get penSensitivity => throw _privateConstructorUsedError;
  double get selectSensitivity => throw _privateConstructorUsedError;
  InputType get inputType => throw _privateConstructorUsedError;
  String? get lastVersion => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ButterflySettingsCopyWith<ButterflySettings> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ButterflySettingsCopyWith<$Res> {
  factory $ButterflySettingsCopyWith(
          ButterflySettings value, $Res Function(ButterflySettings) then) =
      _$ButterflySettingsCopyWithImpl<$Res>;
  $Res call(
      {ThemeMode theme,
      String localeTag,
      String documentPath,
      String dateFormat,
      double touchSensitivity,
      double mouseSensitivity,
      double penSensitivity,
      double selectSensitivity,
      InputType inputType,
      String? lastVersion});
}

/// @nodoc
class _$ButterflySettingsCopyWithImpl<$Res>
    implements $ButterflySettingsCopyWith<$Res> {
  _$ButterflySettingsCopyWithImpl(this._value, this._then);

  final ButterflySettings _value;
  // ignore: unused_field
  final $Res Function(ButterflySettings) _then;

  @override
  $Res call({
    Object? theme = freezed,
    Object? localeTag = freezed,
    Object? documentPath = freezed,
    Object? dateFormat = freezed,
    Object? touchSensitivity = freezed,
    Object? mouseSensitivity = freezed,
    Object? penSensitivity = freezed,
    Object? selectSensitivity = freezed,
    Object? inputType = freezed,
    Object? lastVersion = freezed,
  }) {
    return _then(_value.copyWith(
      theme: theme == freezed
          ? _value.theme
          : theme // ignore: cast_nullable_to_non_nullable
              as ThemeMode,
      localeTag: localeTag == freezed
          ? _value.localeTag
          : localeTag // ignore: cast_nullable_to_non_nullable
              as String,
      documentPath: documentPath == freezed
          ? _value.documentPath
          : documentPath // ignore: cast_nullable_to_non_nullable
              as String,
      dateFormat: dateFormat == freezed
          ? _value.dateFormat
          : dateFormat // ignore: cast_nullable_to_non_nullable
              as String,
      touchSensitivity: touchSensitivity == freezed
          ? _value.touchSensitivity
          : touchSensitivity // ignore: cast_nullable_to_non_nullable
              as double,
      mouseSensitivity: mouseSensitivity == freezed
          ? _value.mouseSensitivity
          : mouseSensitivity // ignore: cast_nullable_to_non_nullable
              as double,
      penSensitivity: penSensitivity == freezed
          ? _value.penSensitivity
          : penSensitivity // ignore: cast_nullable_to_non_nullable
              as double,
      selectSensitivity: selectSensitivity == freezed
          ? _value.selectSensitivity
          : selectSensitivity // ignore: cast_nullable_to_non_nullable
              as double,
      inputType: inputType == freezed
          ? _value.inputType
          : inputType // ignore: cast_nullable_to_non_nullable
              as InputType,
      lastVersion: lastVersion == freezed
          ? _value.lastVersion
          : lastVersion // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$ButterflySettingsCopyWith<$Res>
    implements $ButterflySettingsCopyWith<$Res> {
  factory _$ButterflySettingsCopyWith(
          _ButterflySettings value, $Res Function(_ButterflySettings) then) =
      __$ButterflySettingsCopyWithImpl<$Res>;
  @override
  $Res call(
      {ThemeMode theme,
      String localeTag,
      String documentPath,
      String dateFormat,
      double touchSensitivity,
      double mouseSensitivity,
      double penSensitivity,
      double selectSensitivity,
      InputType inputType,
      String? lastVersion});
}

/// @nodoc
class __$ButterflySettingsCopyWithImpl<$Res>
    extends _$ButterflySettingsCopyWithImpl<$Res>
    implements _$ButterflySettingsCopyWith<$Res> {
  __$ButterflySettingsCopyWithImpl(
      _ButterflySettings _value, $Res Function(_ButterflySettings) _then)
      : super(_value, (v) => _then(v as _ButterflySettings));

  @override
  _ButterflySettings get _value => super._value as _ButterflySettings;

  @override
  $Res call({
    Object? theme = freezed,
    Object? localeTag = freezed,
    Object? documentPath = freezed,
    Object? dateFormat = freezed,
    Object? touchSensitivity = freezed,
    Object? mouseSensitivity = freezed,
    Object? penSensitivity = freezed,
    Object? selectSensitivity = freezed,
    Object? inputType = freezed,
    Object? lastVersion = freezed,
  }) {
    return _then(_ButterflySettings(
      theme: theme == freezed
          ? _value.theme
          : theme // ignore: cast_nullable_to_non_nullable
              as ThemeMode,
      localeTag: localeTag == freezed
          ? _value.localeTag
          : localeTag // ignore: cast_nullable_to_non_nullable
              as String,
      documentPath: documentPath == freezed
          ? _value.documentPath
          : documentPath // ignore: cast_nullable_to_non_nullable
              as String,
      dateFormat: dateFormat == freezed
          ? _value.dateFormat
          : dateFormat // ignore: cast_nullable_to_non_nullable
              as String,
      touchSensitivity: touchSensitivity == freezed
          ? _value.touchSensitivity
          : touchSensitivity // ignore: cast_nullable_to_non_nullable
              as double,
      mouseSensitivity: mouseSensitivity == freezed
          ? _value.mouseSensitivity
          : mouseSensitivity // ignore: cast_nullable_to_non_nullable
              as double,
      penSensitivity: penSensitivity == freezed
          ? _value.penSensitivity
          : penSensitivity // ignore: cast_nullable_to_non_nullable
              as double,
      selectSensitivity: selectSensitivity == freezed
          ? _value.selectSensitivity
          : selectSensitivity // ignore: cast_nullable_to_non_nullable
              as double,
      inputType: inputType == freezed
          ? _value.inputType
          : inputType // ignore: cast_nullable_to_non_nullable
              as InputType,
      lastVersion: lastVersion == freezed
          ? _value.lastVersion
          : lastVersion // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_ButterflySettings extends _ButterflySettings {
  const _$_ButterflySettings(
      {this.theme = ThemeMode.system,
      this.localeTag = '',
      this.documentPath = '',
      this.dateFormat = '',
      this.touchSensitivity = 1,
      this.mouseSensitivity = 1,
      this.penSensitivity = 1,
      this.selectSensitivity = 5,
      this.inputType = InputType.multiDraw,
      this.lastVersion})
      : super._();

  @JsonKey()
  @override
  final ThemeMode theme;
  @JsonKey()
  @override
  final String localeTag;
  @JsonKey()
  @override
  final String documentPath;
  @JsonKey()
  @override
  final String dateFormat;
  @JsonKey()
  @override
  final double touchSensitivity;
  @JsonKey()
  @override
  final double mouseSensitivity;
  @JsonKey()
  @override
  final double penSensitivity;
  @JsonKey()
  @override
  final double selectSensitivity;
  @JsonKey()
  @override
  final InputType inputType;
  @override
  final String? lastVersion;

  @override
  String toString() {
    return 'ButterflySettings(theme: $theme, localeTag: $localeTag, documentPath: $documentPath, dateFormat: $dateFormat, touchSensitivity: $touchSensitivity, mouseSensitivity: $mouseSensitivity, penSensitivity: $penSensitivity, selectSensitivity: $selectSensitivity, inputType: $inputType, lastVersion: $lastVersion)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ButterflySettings &&
            const DeepCollectionEquality().equals(other.theme, theme) &&
            const DeepCollectionEquality().equals(other.localeTag, localeTag) &&
            const DeepCollectionEquality()
                .equals(other.documentPath, documentPath) &&
            const DeepCollectionEquality()
                .equals(other.dateFormat, dateFormat) &&
            const DeepCollectionEquality()
                .equals(other.touchSensitivity, touchSensitivity) &&
            const DeepCollectionEquality()
                .equals(other.mouseSensitivity, mouseSensitivity) &&
            const DeepCollectionEquality()
                .equals(other.penSensitivity, penSensitivity) &&
            const DeepCollectionEquality()
                .equals(other.selectSensitivity, selectSensitivity) &&
            const DeepCollectionEquality().equals(other.inputType, inputType) &&
            const DeepCollectionEquality()
                .equals(other.lastVersion, lastVersion));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(theme),
      const DeepCollectionEquality().hash(localeTag),
      const DeepCollectionEquality().hash(documentPath),
      const DeepCollectionEquality().hash(dateFormat),
      const DeepCollectionEquality().hash(touchSensitivity),
      const DeepCollectionEquality().hash(mouseSensitivity),
      const DeepCollectionEquality().hash(penSensitivity),
      const DeepCollectionEquality().hash(selectSensitivity),
      const DeepCollectionEquality().hash(inputType),
      const DeepCollectionEquality().hash(lastVersion));

  @JsonKey(ignore: true)
  @override
  _$ButterflySettingsCopyWith<_ButterflySettings> get copyWith =>
      __$ButterflySettingsCopyWithImpl<_ButterflySettings>(this, _$identity);
}

abstract class _ButterflySettings extends ButterflySettings {
  const factory _ButterflySettings(
      {ThemeMode theme,
      String localeTag,
      String documentPath,
      String dateFormat,
      double touchSensitivity,
      double mouseSensitivity,
      double penSensitivity,
      double selectSensitivity,
      InputType inputType,
      String? lastVersion}) = _$_ButterflySettings;
  const _ButterflySettings._() : super._();

  @override
  ThemeMode get theme;
  @override
  String get localeTag;
  @override
  String get documentPath;
  @override
  String get dateFormat;
  @override
  double get touchSensitivity;
  @override
  double get mouseSensitivity;
  @override
  double get penSensitivity;
  @override
  double get selectSensitivity;
  @override
  InputType get inputType;
  @override
  String? get lastVersion;
  @override
  @JsonKey(ignore: true)
  _$ButterflySettingsCopyWith<_ButterflySettings> get copyWith =>
      throw _privateConstructorUsedError;
}
