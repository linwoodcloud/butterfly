- - -
title: Bereich
- - -

## Einführung

Flächen sind eine Möglichkeit, die Größe der Leinwand zu beschränken. Sie können Flächen mit dem [-Flächenmaler](painters/area.md) erstellen. Wenn Sie auf den Hintergrund klicken, können Sie den Bereichslisten-Dialog über das Kontextmenü öffnen.

TODO: Fügen Sie weitere Informationen zu Bereichen hinzu.
