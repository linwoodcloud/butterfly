---
title: "Android"
sidebar_position: 4
---

![Stable Release-Version](https://img.shields.io/badge/dynamic/yaml?color=c4840d&label=Stable&query=%24.version&url=https%3A%2F%2Fraw.githubusercontent.com%2FLinwoodCloud%2Fbutterfly%2Fstable%2Fapp%2Fpubspec.yaml&style=for-the-badge) ![Nachts Release Version](https://img.shields.io/badge/dynamic/yaml?color=f7d28c&label=Nightly&query=%24.version&url=https%3A%2F%2Fraw.githubusercontent.com%2FLinwoodCloud%2Fbutterfly%2Fnightly%2Fapp%2Fpubspec.yaml&style=for-the-badge)

:::warning

Verwenden Sie nicht die nächtliche Version von Butterfly für die Produktion.

:::

## Binärdateien

<div className="row margin-bottom--lg padding--sm">
<a class="button button--outline button--info button--lg margin--sm" href="https://github.com/LinwoodCloud/butterfly/releases/download/stable/app-release.apk">
  Stabil
</a>
<a class="button button--outline button--danger button--lg margin--sm" href="https://github.com/LinwoodCloud/butterfly/releases/download/nightly/app-release.apk">
  Nachts
</a>
</div>

## Filialen

<div className="row margin-bottom--lg padding--sm">
<a class="button button--outline button--primary button--lg margin--sm" href="https://play.google.com/store/apps/details?id=dev.linwood.butterfly">
  Play Store
</a>
<a class="button button--outline button--primary button--lg margin--sm" href="https://f-droid.org/de/packages/dev.linwood.butterfly">
  F-Droid
</a>
</div>

## Das Beta-Programm im Play Store

Klicken Sie auf [diesen Link](https://play.google.com/apps/testing/dev.linwood.butterfly) um sich für die [nächtlich](/nightly) Version der App zu registrieren. Bitte beachten Sie, dass diese Version Bugs haben kann. Die Play Store-Version wird automatisch aktualisiert, wenn eine neue nächtliche Version veröffentlicht wird.
