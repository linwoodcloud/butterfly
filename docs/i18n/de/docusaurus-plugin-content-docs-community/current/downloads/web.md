---
title: "Web"
sidebar_position: 5
---

![Stable Release-Version](https://img.shields.io/badge/dynamic/yaml?color=c4840d&label=Stable&query=%24.version&url=https%3A%2F%2Fraw.githubusercontent.com%2FLinwoodCloud%2Fbutterfly%2Fstable%2Fapp%2Fpubspec.yaml&style=for-the-badge) ![Nachts Release Version](https://img.shields.io/badge/dynamic/yaml?color=f7d28c&label=Nightly&query=%24.version&url=https%3A%2F%2Fraw.githubusercontent.com%2FLinwoodCloud%2Fbutterfly%2Fnightly%2Fapp%2Fpubspec.yaml&style=for-the-badge)

:::warning

Verwenden Sie nicht die nächtliche Version von Butterfly für die Produktion.

:::

:::note

Aus Performancegründen sollten Sie lieber die native Version anstelle der Webversion verwenden.

:::

## Links

<div className="row margin-bottom--lg padding--sm">
<a class="button button--outline button--info button--lg margin--sm" href="https://butterfly.linwood.dev">
  Stabil
</a>
<a class="button button--outline button--danger button--lg margin--sm" href="https://preview.butterfly.linwood.dev">
  Nachts
</a>
</div>
