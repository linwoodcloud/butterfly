---
slug: "/downloads"
sidebar_position: 0
title: Downloads
---

![Stable Release-Version](https://img.shields.io/badge/dynamic/yaml?color=c4840d&label=Stable&query=%24.version&url=https%3A%2F%2Fraw.githubusercontent.com%2FLinwoodCloud%2Fbutterfly%2Fstable%2Fapp%2Fpubspec.yaml&style=for-the-badge) ![Nachts Release Version](https://img.shields.io/badge/dynamic/yaml?color=f7d28c&label=Nightly&query=%24.version&url=https%3A%2F%2Fraw.githubusercontent.com%2FLinwoodCloud%2Fbutterfly%2Fnightly%2Fapp%2Fpubspec.yaml&style=for-the-badge)

## Wähle deine Plattform

:::note

Aus Performancegründen sollten Sie lieber die native Version anstelle der Webversion verwenden.

:::

<div className="row margin-bottom--lg padding--sm">
<a class="button button--outline button--primary button--lg margin--sm" href="/downloads/windows">
  Windows
</a>
<a class="button button--outline button--primary button--lg margin--sm" href="/downloads/linux">
  Linux
</a>
<a class="button button--outline button--info button--lg margin--sm" href="/downloads/android">
  Android
</a>
<a class="button button--outline button--danger button--lg margin--sm" href="/downloads/web">
  Web
</a>
</div>

Oder du möchtest [deine eigene](/build-your-own) bauen

## Nützliche Links

- [Änderungsverlauf](changelog)
- [Ältere Versionen](https://github.com/LinwoodCloud/butterfly/releases)
- [Neueste Version](https://github.com/LinwoodCloud/butterfly/releases/latest)
- [Unterstützung](https://discord.linwood.dev)