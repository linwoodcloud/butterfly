---
slug: "/downloads"
sidebar_position: 0
title: Téléchargements
---

![Version de la version stable](https://img.shields.io/badge/dynamic/yaml?color=c4840d&label=Stable&query=%24.version&url=https%3A%2F%2Fraw.githubusercontent.com%2FLinwoodCloud%2Fbutterfly%2Fstable%2Fapp%2Fpubspec.yaml&style=for-the-badge) ![Version de la sortie nocturne](https://img.shields.io/badge/dynamic/yaml?color=f7d28c&label=Nightly&query=%24.version&url=https%3A%2F%2Fraw.githubusercontent.com%2FLinwoodCloud%2Fbutterfly%2Fnightly%2Fapp%2Fpubspec.yaml&style=for-the-badge)

## Choisissez votre plateforme

:::note

Pour des raisons de performance, préférez utiliser la version native plutôt que la version web.

:::

<div className="row margin-bottom--lg padding--sm">
<a class="button button--outline button--primary button--lg margin--sm" href="/downloads/windows">
  Windows
</a>
<a class="button button--outline button--primary button--lg margin--sm" href="/downloads/linux">
  Linux
</a>
<a class="button button--outline button--info button--lg margin--sm" href="/downloads/android">
  Android
</a>
<a class="button button--outline button--danger button--lg margin--sm" href="/downloads/web">
  Web
</a>
</div>

Ou vous voulez [construire votre propre](/build-your-own)

## Liens utiles

- [Historique des modifications](changelog)
- [Anciennes versions](https://github.com/LinwoodCloud/butterfly/releases)
- [Dernière version](https://github.com/LinwoodCloud/butterfly/releases/latest)
- [Soutien](https://discord.linwood.dev)