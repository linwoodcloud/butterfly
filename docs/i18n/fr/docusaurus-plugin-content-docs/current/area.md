- - -
title: Zone d'application
- - -

## Introduction

Les zones sont un moyen de limiter la taille de la toile. Vous pouvez créer des zones en utilisant le peintre d'aire [](painters/area.md). Si vous cliquez sur l'arrière-plan, vous pouvez ouvrir la boîte de dialogue de la liste des zones en utilisant le menu contextuel.

TODO : Ajouter plus d'informations sur les zones.
