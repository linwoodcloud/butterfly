---
title: "Web"
sidebar_position: 5
---

![Stable release version](https://img.shields.io/badge/dynamic/yaml?color=c4840d&label=Stable&query=%24.version&url=https%3A%2F%2Fraw.githubusercontent.com%2FLinwoodCloud%2Fbutterfly%2Fstable%2Fapp%2Fpubspec.yaml&style=for-the-badge)
![Nightly release version](https://img.shields.io/badge/dynamic/yaml?color=f7d28c&label=Nightly&query=%24.version&url=https%3A%2F%2Fraw.githubusercontent.com%2FLinwoodCloud%2Fbutterfly%2Fnightly%2Fapp%2Fpubspec.yaml&style=for-the-badge)

:::warning

Do not use the nightly version of Butterfly for production.

:::

:::note

For performance reasons, prefer using the native version instead of the web version.

:::

## Links

<div className="row margin-bottom--lg padding--sm">
<a class="button button--outline button--info button--lg margin--sm" href="https://butterfly.linwood.dev">
  Stable
</a>
<a class="button button--outline button--danger button--lg margin--sm" href="https://preview.butterfly.linwood.dev">
  Nightly
</a>
</div>
