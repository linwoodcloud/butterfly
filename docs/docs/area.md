---
title: Area
---

## Introduction

Areas are a way to restrict the size of the canvas. You can create areas by using the [area painter](painters/area.md). If you click on the background you can open the area list dialog using the context menu.

TODO: Add more information about areas.
